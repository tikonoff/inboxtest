exports.task = {
  dist: {
    options: {
      style: 'expanded',
      lineNumbers: true, // 1
      sourcemap: 'none'
    },
    files: [{
      expand: true, // 2
      cwd: '../src/css/sass',
      src: [ '**/*.scss' ],
      dest: '../src/css',
      ext: '.css'
    }]
  }
};