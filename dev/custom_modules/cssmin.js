exports.task = {
  my_target: {
    files: [{
      expand: true,
      cwd: '../src/css/',
      src: [ '*.css', '!*.min.css' ], // 1
      dest: '../src/css/',
      ext: '.min.css'
    }]
  }
};